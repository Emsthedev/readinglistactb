// Activity: Arrow Functions, Selection Control Structures, For Loop, JSON in Javascript



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: 
	Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85.
	 You have received a Developing" mark.

*/

// Code here:
console.log("Number 1");


let getRemarks = (avg) => {
	if (avg <= 74) {
		console.log(`Your quarterly average is ${avg}. 
	  				We are sorry to say that you have received a "Failed" mark.`);
	} else if (avg >= 75 && avg <= 80) {
		console.log(`Congratulations! Your quarterly average is ${avg}. 
					You have received a "Beginner" mark.`);
	} else if (avg >= 81 && avg <= 85) {
		console.log(`Congratulations! Your quarterly average is ${avg}. 
					You have received a "Developing" mark.`);
	} else if (avg >= 86 && avg <= 90) {
		console.log(`Congratulations! Your quarterly average is ${avg}. 
					You have received  "Above Average" mark.`);
	} else {
		console.log(`Congratulations! Your quarterly average is ${avg}. 
					You have received  "Advanced" mark.`);
	}
};
getRemarks(73);
getRemarks(74);

getRemarks(75);
getRemarks(80);

getRemarks(81);
getRemarks(85);

getRemarks(86);
getRemarks(90)

getRemarks(91);
getRemarks(92);






/*	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:
console.log("");
console.log("Number 2");
for (let s = 1; s <= 300; s++) {
   
	if(s % 2 === 0){
		console.log(`${s} - Even`)
		continue;
	}
	console.log(`${s} - Odd`)
	if (s > 300){
		break;
	}
}



/*
	3. Create a an object named ""hero"" and input the details using promp(). 
	Here are the details needed: heroName, origin, description, skills(object which will contain 
		3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/
//Code:
console.log("");
console.log("Number 3");
console.log("");
let heroName =  prompt("What is your hero's name? ")
let origin =  prompt("Birthplace of hero? ")
let description = prompt("Add a description of hero ")
let skills = {
	skill1: prompt("What is your Skill 1 "),
	skills2 : prompt("What is your Skill 2 "),
	skills3 :prompt("What is your Skill 3 ")

}


let hero = ({
	heroName : heroName,
	origin : origin,
	description : description,
	skills : skills

})

console.log(`Result JS Objects :`)
console.log(JSON.stringify(hero))



